# Singly Linked List

Lista con un solo apuntador

```bash
$ node index.js   
```

> Instrucciones 

```js
const sL = new SinglyLinkedList(2);
sL.print()

sL.append(4)
sL.append(5)
sL.print()

sL.prepend(1)
sL.prepend(0)
sL.print()

sL.insert(3, 3)
sL.print()

sL.insert(-1, 0)
sL.print()

sL.remove(4)
sL.print()
```

> Salida

```
[1] = 2
[3] = 2 -> 4 -> 5
[5] = 0 -> 1 -> 2 -> 4 -> 5
[6] = 0 -> 1 -> 2 -> 3 -> 4 -> 5
[7] = -1 -> 0 -> 1 -> 2 -> 3 -> 4 -> 5
[6] = -1 -> 0 -> 1 -> 2 -> 4 -> 5

```