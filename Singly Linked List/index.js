class ListNode {
  constructor(value) {
    this.pointer = null
    this.value = value
  }
}

class SinglyLinkedList {
  constructor(value) {
    this.head = new ListNode(value)
    this.tail = this.head
    this.length = 1
    return this
  }

  append(value) {
    const tmpNode = new ListNode(value)
    this.tail.pointer = tmpNode
    this.tail = tmpNode
    this.length += 1

    return this  
  }

  prepend(value) {
    const tmpNode = new ListNode(value)
    tmpNode.pointer = this.head
    this.head = tmpNode
    this.length += 1

    return this
  }

  insert(value, position) {
    this.validateScope(position)
    if (position === 0) {
      this.prepend(value)
    }
    else if (position === this.length) {
      this.append(value)
    } else {
      const tmpNode = this.getNodeByIndex(position -1 )
      const newNode = new ListNode(value)
      newNode.pointer = tmpNode.pointer
      tmpNode.pointer = newNode
      this.length += 1
    }

    return this
  }

  remove(position) {
    this.validateScope(position)
    if (position === 0) {
      this.head = this.head.pointer
    } else {
      const tmpNode = this.getNodeByIndex(position-1)
      const next = tmpNode.pointer.pointer
      tmpNode.pointer = next
    }
    this.length -= 1
  }

  getNodeByIndex(index) {
    let tmpNode = this.head
    for (let counter = 0; counter < index; counter++) {
      tmpNode = tmpNode.pointer
    }

    return tmpNode
  }

  validateScope(position) {
    if (position < 0 || position > this.length) throw new Error(`No puede agregar fuera de scope de la lista, intente con un número entre 0 y ${this.length}`)
  }

  print(v=undefined, str='') {
    if (v === undefined) {
      const str = `${this.head.value}`;
      const info = this.print(this.head.pointer, str);
      console.log(`[${this.length}] = ${info}`)
    } else if (v !== null) {
      const tmp = `${str} -> ${v.value}`;
      return this.print(v.pointer, tmp);
    } else {
      return str
    }
  }
}


const sL = new SinglyLinkedList(2);
sL.print()

sL.append(4)
sL.append(5)
sL.print()

sL.prepend(1)
sL.prepend(0)
sL.print()

sL.insert(3, 3)
sL.print()

sL.insert(-1, 0)
sL.print()

sL.remove(4)
sL.print()


