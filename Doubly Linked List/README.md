# Doubly Linked List

Lista con dos apuntadores

```bash
$ node index.js   
```

> Instrucciones 

```js
const sL = new DoublyLinkedList(2);
sL.print()

sL.append(4)
sL.append(5)
sL.print()

sL.prepend(1)
sL.prepend(0)
sL.print()

sL.insert(3, 3)
sL.insert(6, 3)
sL.print()

sL.remove(3)
sL.print()
```

> Salida

```
[1] = 2
[3] = 2 <-> 4 <-> 5
[5] = 0 <-> 1 <-> 2 <-> 4 <-> 5
[7] = 0 <-> 1 <-> 2 <-> 6 <-> 3 <-> 4 <-> 5
[6] = 0 <-> 1 <-> 2 <-> 3 <-> 4 <-> 5

```