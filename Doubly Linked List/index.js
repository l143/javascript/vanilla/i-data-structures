class ListNode {
  constructor(value) {
    this.next = null
    this.prev = null
    this.value = value
  }
}

class DoublyLinkedList {
  constructor(value) {
    this.head = new ListNode(value)
    this.tail = this.head
    this.length = 1
    return this
  }

  append(value) {
    const tmpNode = new ListNode(value)
    tmpNode.prev = this.tail
    this.tail.next = tmpNode
    this.tail = tmpNode
    this.length += 1

    return this  
  }

  prepend(value) {
    const tmpNode = new ListNode(value)
    tmpNode.next = this.head
    this.head.prev = tmpNode
    this.head = tmpNode
    this.length += 1

    return this
  }

  insert(value, position) {
    this.validateScope(position)
    if (position === 0) {
      this.prepend(value)
    }
    else if (position === this.length) {
      this.append(value)
    } else {
      const tmpNode = this.getNodeByIndex(position)
      const newNode = new ListNode(value)
      newNode.prev = tmpNode.prev
      newNode.next = tmpNode
      tmpNode.prev = newNode
      newNode.prev.next = newNode

      this.length += 1
    }

    return this
  }

  remove(position) {
    this.validateScope(position)
    if (position === 0) {
      this.head = this.head.next
      this.head.prev = null
    } else if ( position === this.length) {
      this.tail = this.tail.prev
      this.tail.next = null
    } else {
      const tmpNode = this.getNodeByIndex(position)
      tmpNode.prev.next = tmpNode.next
      tmpNode.next.prev = tmpNode.prev
      tmpNode.next = null
      tmpNode.prev = null
    }
    this.length -= 1
  }

  getNodeByIndex(index) {
    let tmpNode = this.head
    for (let counter = 0; counter < index; counter++) {
      tmpNode = tmpNode.next
    }

    return tmpNode
  }

  validateScope(position) {
    if (position < 0 || position > this.length) throw new Error(`No puede agregar fuera de scope de la lista, intente con un número entre 0 y ${this.length}`)
  }

  print(v=undefined, str='') {
    if (v === undefined) {
      const str = `${this.head.value}`;
      const info = this.print(this.head.next, str);
      console.log(`[${this.length}] = ${info}`)
    } else if (v !== null) {
      const tmp = `${str} <-> ${v.value}`;
      return this.print(v.next, tmp);
    } else {
      return str
    }
  }
}


const sL = new DoublyLinkedList(2);
sL.print()

sL.append(4)
sL.append(5)
sL.print()

sL.prepend(1)
sL.prepend(0)
sL.print()

sL.insert(3, 3)
sL.insert(6, 3)
sL.print()

sL.remove(3)
sL.print()
